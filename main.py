# main.py

class Person:
    
    def __init__(self, name, age, employ):
        self.name = name
        self.age = age
        self.employ = employ
        self.posX = 0
        self.posY = 0
        
    def __str__(self):
        return f"{self.name} have {self.age} years and is a {self.employ}"
    
    def currentPosition(self):
        return f"{self.name} is on position {self.posX}, {self.posY}"
        
    def changePosition(self, newposX, newposY):
        self.posX = self.posX + newposX
        self.posY = self.posY + newposY
        return self.posX, self.posY

class SantaClaus(Person):
    # I get to a variable in another unrelated class
    def givePresents(self, present, workshopInstance):
        workshopInstance.presents.remove(present)

class Elf(Person):
    ...

class Home:
    def __init__(self, name, address):
        self.name = name
        self.address = address
    
    def __str__(self):
        return f"The building {self.name} at {self.address}"

class SantasHouse(Home):        
    def __str__(self):
        return f"This is {self.name} house"

class Workshop(Home):
    presents = []
    
    def __str__(self):
        return f"The address {self.name} is {self.address}"
    
    def showPresentsList(self):
        return f"Gift list:\n{self.presents}"
    
    def createPresent(self, present):
        self.presents.append(present)
        return f"{present} the gift has been created"

class ElfHouse(Home):
    def __str__(self):
        return f"{self.name} is on {self.address}"
    
def main():
    # Create Santa Claus
    santa = SantaClaus("Santa Claus", 2024, "Xmass Maker")
    print(santa)
    # Change position of Santa Claus
    print(santa.currentPosition())
    santa.changePosition(24, 5)
    print(santa.currentPosition())
    santa.changePosition(2, 2)
    print(santa.currentPosition())
    
    # Create Elf
    marek = Elf("Elf Marek", 856, "Toys Maker")
    print(marek)
    # Change position of Elf
    marek.changePosition(300, 45)
    print(marek.currentPosition())
    marek.changePosition(34, -8)
    print(marek.currentPosition())
    
    # Create Elf'a house
    marekHouse = ElfHouse("Elf Marek's House", "Santa Street 278")
    print(marekHouse)
    
    # Create Toy's Workshop
    toysWorkshop = Workshop("Toy's Workshop", "Santa Sreet 1")
    print(toysWorkshop)
    # Create gitft and add to Present List
    toysWorkshop.createPresent("Piłka")
    # Show Present's List
    print(toysWorkshop.showPresentsList())
    toysWorkshop.createPresent("Kolejka")
    print(toysWorkshop.showPresentsList())
    
    # Santa Claus gives a present and removes it from the list
    santa.givePresents("Piłka", toysWorkshop)
    print(toysWorkshop.showPresentsList())
    
    
if __name__ == "__main__":
    main()